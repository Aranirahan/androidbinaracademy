class Student(val age: Int) {
    private val name = "Khamdan Nahari"
    private val nim = 16650068

    init {
        showPerson()
    }

    fun showPerson() {
        println("name : $name")
        println("nim : $nim")
        println("age : $age")
    }

    fun showPersonFromDataClass() {
        val person1 = Person("aran", 16650068, age)
        println("name : ${person1.name}")
        println("nim : ${person1.nim}")
        println("age : ${person1.age}")
    }
}

class Animal(val animalName: String, val crow: String) {

    fun crowingNow() {
        println("The $animalName crow ${crow.toUpperCase()}...")
    }
}

class Calculator {

    fun divide(numberA: Int, numberB: Int) = numberA / numberB

    fun decrease(numberA: Int, numberB: Int) = numberA - numberB

    fun increase(vararg x: Int) = x.sum()

    fun probably(numberA: Int, numberB: Int): Int {
        return numberA * numberB
    }
}

fun main(args: Array<String>) {
    val student1 = Student(21)
    student1.showPerson()
    student1.showPersonFromDataClass()

    val animal1 = Animal("Cat", "miaomiao")
    animal1.crowingNow()

    val calculator1 = Calculator()
    val devideResult = calculator1.divide(3, 1)
    val decreaseResult = calculator1.decrease(3, 1)
    val increaseResult = calculator1.increase(3, 1, 2, 3, 4, 2, 2)
    val probablyResult = calculator1.probably(3, 1)
    println(devideResult)
    println(decreaseResult)
    println(increaseResult)
    println(probablyResult)


}

data class Person(
    val name: String,
    val nim: Int,
    val age: Int
)


