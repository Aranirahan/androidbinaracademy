
fun main(args: Array<String>) {
    val data = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    val fruitMutableList = mutableListOf("mangga", "jeruk", "pisang")

    showNumberList(data)
    showGroup(7)
    showFruitMutableList(fruitMutableList)
    showFruitMutableListDown(fruitMutableList)
}

fun showNumberList(data: List<Int>) {
    data.forEach {
        if (it % 2 != 0)
            println("odd number $it")
        else
            println("even number $it")
    }
}

fun showGroup(number: Int){
    val huruf: String
    huruf = when {
        number >= 8 -> "A"
        number >= 6 -> "B"
        number >= 4 -> "C"
        else -> "D"
    }
    println(huruf)
}

fun showFruitMutableList(data: List<String>){
   data.forEach{
       println(it)
   }
}

fun showFruitMutableListDown(data: List<String>){
   for (i in data.size-1 downTo 0 ){
       println(data[i])
   }
}

//list : size fix, value item fix
//array : size fix, value item flexible
//mutablelist : size flexble, value item flexible

//step : loncat
//until : index paling belakang tidak terbaca
//downTo : menurun